<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  
  <form action="" method="POST">
    <table>
      <tr>
        <td>Nama</td>
        <td>:</td>
        <td><input type="text" name="nama" style="width:190px"></td>
      </tr>
      <tr>
        <td>Kode Jurusan</td>
        <td>:</td>
        <td>
          <select name="jurusan" style="width:200px">
            <option value="TI2018213">TI2018213</option>
            <option value="MI2019239">MI2019239</option>
            <option value="HK2019980">HK2019980</option>
            <option value="AK2018012">AK2018012</option>
          </select>
        </td>
      </tr>
      <tr>
        <td>Kode Mata Kuliah</td>
        <td>:</td>
        <td>
          <select name="matkul" style="width:200px">
            <option value="MKDU02">MKDU02</option>
            <option value="MKPI02">MKPI02</option>
            <option value="MKIN03">MKIN03</option>
          </select>
        </td>
      </tr>
    </table>

    <input type="submit" value="Hitung" name="hitung">
  </form>

  <br>

  <?php
  $proses = @$_POST['hitung'];
  $nama = @$_POST['nama'];
  $jurusan = @$_POST['jurusan'];
  $matkul = @$_POST['matkul'];

  $kode_jurusan = substr($jurusan, 0, 2);
  $tahun = substr($jurusan, 2, 4);
  $no_urut = substr($jurusan, -3);

  $kode_matkul = substr($matkul, 2, 2);
  $sks = substr($matkul, -1);

  $nama_jurusan = "";
  $nama_matkul = "";

  if ($proses) {

    switch ($kode_jurusan) {
      case "TI":
        $nama_jurusan = "Teknik Informatika";
        break;
      case "MI":
        $nama_jurusan = "Manajemen Informatika";
        break;
      case "HK":
        $nama_jurusan = "Hukum";
        break;
      case "AK":
        $nama_jurusan = "Akuntansi";
        break;
      default:
        $nama_jurusan = "";
        break;
    }

    switch ($kode_matkul) {
      case "DU":
        $nama_matkul = "Dasar Umum";
        break;
      case "IN":
        $nama_matkul = "Inti";
        break;
      case "PI":
        $nama_matkul = "Piihan";
        break;
      default:
        $nama_matkul = "";
        break;
    }

  ?>

    <table>
      <tr>
        <td>Nama</td>
        <td>:</td>
        <td><?php echo strtoupper($nama); ?></td>
        <td>Kode Jurusan</td>
        <td>:</td>
        <td><?php echo strtoupper($kode_jurusan); ?></td>
      </tr>
      <tr>
        <td>Nama Jurusan</td>
        <td>:</td>
        <td><?php echo $nama_jurusan; ?></td>
        <td>Tahun Angkatan</td>
        <td>:</td>
        <td><?php echo $tahun; ?></td>
      </tr>
      <tr>
        <td>No. Urut</td>
        <td>:</td>
        <td><?php echo $no_urut; ?></td>
        <td>Kode Mata Kuliah</td>
        <td>:</td>
        <td><?php echo $kode_matkul; ?></td>
      </tr>
      <tr>
        <td>Mata Kuliah</td>
        <td>:</td>
        <td><?php echo $nama_matkul; ?></td>
        <td>SKS</td>
        <td>:</td>
        <td><?php echo $sks; ?></td>
      </tr>
    </table>
  <?php
  }
  ?>
</body>

</html>